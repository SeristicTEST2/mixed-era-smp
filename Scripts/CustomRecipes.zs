import crafttweaker.item.IIngredient;
import crafttweaker.item.IItemStack;
import crafttweaker.oredict.IOreDict;
import crafttweaker.oredict.IOreDictEntry;
import mods.thermalexpansion.Pulverizer;

//Variables
var woodplank = <ore:plankWood>;
val logs = <ore:logWood>;
val ingot_cast = <tconstruct:cast_custom:0>;
val roseGoldDust = <mca:rose_gold_dust>;
val roseGoldOre = <mca:rose_gold_ore>;
val goldDust = <mca:gold_dust>;
val stygianOre = <woot:stygianironore>;
val stygianDust = <woot:stygianirondust>;
val stygianIngot = <woot:stygianironingot>;
val oreRainbow = <goodnightsleep:rainbow_ore>;
val ingotRainbow = <goodnightsleep:rainbow_ingot>;
val ingotZitrite = <goodnightsleep:zitrite_ingot>;
val oreZitrite = <goodnightsleep:zitrite_ore>;
val orePoorZinc = <railcraft:ore_metal_poor:7>;
val nuggetPoorZinc = <railcraft:nugget:8>;
val ingotZinc = <railcraft:ingot:8>;
val oreZinc = <railcraft:ore_metal:5>;
val orePoorSilver = <railcraft:ore_metal_poor:5>;
val orePoorLead = <railcraft:ore_metal_poor:4>;
val orePoorGold = <railcraft:ore_metal_poor:1>;
val orePoorCopper = <railcraft:ore_metal_poor:2>;
val orePoorIron = <railcraft:ore_metal_poor:0>;
val orePoorTin = <railcraft:ore_metal_poor:3>;
val orePoorNickel = <railcraft:ore_metal_poor:6>;
val nuggetSilver = <thermalfoundation:material:194>;
val nuggetLead = <thermalfoundation:material:195>; 
val nuggetGold = <minecraft:gold_nugget>;
val nuggetCopper = <thermalfoundation:material:192>;
val nuggetIron = <minecraft:iron_nugget>;
val nuggetTin = <thermalfoundation:material:193>;
val nuggetNickel = <thermalfoundation:material:197>;

//Variables

//Vanilla

//Vanilla

//Blaze Rod Burn Timers
furnace.setFuel(<minecraft:blaze_rod>, 2800);
//Blaze Rod Burn Timers

//Bag of Holding Error
<extrautils2:bagofholding>.addTooltip(
    format.underline(format.red(
        "Becareful when using, switching to Creative > Survival could cause loss of items."
    ))
);
//Bag of Holding Error

//Iron Chest
##Iron Chest
recipes.remove(<ironchest:iron_chest:0>);
recipes.addShaped("Iron Chest", <ironchest:iron_chest>, 
[[<minecraft:iron_ingot>, <minecraft:iron_ingot>, <minecraft:iron_ingot>],
[<minecraft:iron_ingot>, <minecraft:chest>, <minecraft:iron_ingot>],
[<minecraft:iron_ingot>, <minecraft:iron_ingot>, <minecraft:iron_ingot>]]);

recipes.addShapeless("Iron Ingot", <minecraft:iron_ingot> * 8, [<ironchest:iron_chest>]);
##Iron Chest
##Silver Chest
recipes.remove(<ironchest:iron_chest:4>);
recipes.addShaped("Silver Chest", <ironchest:iron_chest:4>, 
[[<thermalfoundation:material:130>, <thermalfoundation:material:130>, <thermalfoundation:material:130>],
[<thermalfoundation:material:130>, <minecraft:chest>, <thermalfoundation:material:130>],
[<thermalfoundation:material:130>, <thermalfoundation:material:130>, <thermalfoundation:material:130>]]);

recipes.addShapeless("Silver Ingot", <thermalfoundation:material:130>*8, [<ironchest:iron_chest:4>]);
##Silver Chest
##Copper Chest
recipes.remove(<ironchest:iron_chest:3>);
recipes.addShaped("Copper Chest", <ironchest:iron_chest:3>, 
[[<thermalfoundation:material:128>, <thermalfoundation:material:128>, <thermalfoundation:material:128>],
[<thermalfoundation:material:128>, <minecraft:chest>, <thermalfoundation:material:128>],
[<thermalfoundation:material:128>, <thermalfoundation:material:128>, <thermalfoundation:material:128>]]);

recipes.addShapeless(<thermalfoundation:material:128>* 8, [<ironchest:iron_chest:3>]);
##Copper Chest
##Golden Chest
recipes.remove(<ironchest:iron_chest:1>);
recipes.addShaped("Gold Chest", <ironchest:iron_chest:1>, 
[[<minecraft:gold_ingot>, <minecraft:gold_ingot>, <minecraft:gold_ingot>],
[<minecraft:gold_ingot>, <ironchest:iron_chest:1>, <minecraft:gold_ingot>],
[<minecraft:gold_ingot>, <minecraft:gold_ingot>, <minecraft:gold_ingot>]]);

recipes.addShapeless(<minecraft:gold_ingot>* 8, [<ironchest:iron_chest:1>]);
##Golden Chest
##Diamond Chest
recipes.remove(<ironchest:iron_chest:2>);
recipes.addShaped("Diamond Chest", <ironchest:iron_chest:2>, 
[[<minecraft:diamond>, <minecraft:glass>, <minecraft:diamond>],
[<minecraft:iron_ingot>, <ironchest:iron_chest:1>, <minecraft:iron_ingot>],
[<minecraft:diamond>, <minecraft:diamond>, <minecraft:diamond>]]);

recipes.addShapeless(<minecraft:diamond>* 3, [<ironchest:iron_chest:2>]);
##Diamond Chest
##Crystal Chest
recipes.remove(<ironchest:iron_chest:5>);
recipes.addShaped("Crystal Chest", <ironchest:iron_chest:5>, 
[[<minecraft:glass>, <minecraft:glass>, <minecraft:glass>],
[<minecraft:glass>, <ironchest:iron_chest:2>, <minecraft:glass>],
[<minecraft:glass>, <minecraft:glass>, <minecraft:glass>]]);

recipes.addShapeless(<minecraft:glass>*8, [<ironchest:iron_chest:5>]);
##Crystal Chest
//Iron Chest

//Conflicting Recipes

//Iron Gear Recipe
recipes.remove(<thermalfoundation:material:24>);
recipes.addShaped("Iron Gear", <thermalfoundation:material:24>, 
[[null, <minecraft:iron_ingot>, null],
[<minecraft:iron_ingot>, <ore:gearStone>, <minecraft:iron_ingot>],
[null, <minecraft:iron_ingot>, null]]);
//Iron Gear Recipe
//Conflicting Recipes

//Broken Ores

//Rose Gold (MCA)
mods.thermalexpansion.Pulverizer.addRecipe(roseGoldDust * 2, roseGoldOre, 1500, goldDust, 20);
//Rose Gold (MCA)
//Stygian Iron
mods.thermalexpansion.Pulverizer.addRecipe(stygianDust * 2, stygianOre, 1500);
//Stygian Iron
//IC2 Uranium
furnace.addRecipe(<ic2:ingot:8>, <ic2:resource:4>);
//IC2 Uranium
//Monazit Ore
mods.thermalexpansion.Pulverizer.addRecipe(<modularforcefieldsystem:forcicium> * 4, <modularforcefieldsystem:monazit_ore>, 1500);
//Monazit Ore

//Rainbow ore
mods.thermalexpansion.Pulverizer.addRecipe(ingotRainbow * 2, oreRainbow, 1500);
//Rainbow ore

//Zitrite ore
mods.thermalexpansion.Pulverizer.addRecipe(ingotZitrite *2, oreZitrite, 1500);
//Zitrite ore

//Poor zinc ore
mods.thermalexpansion.Pulverizer.addRecipe(nuggetPoorZinc *2, orePoorZinc, 1500);
//Poor zinc ore

//Zinc Ore
mods.thermalexpansion.Pulverizer.addRecipe(ingotZinc *2, oreZinc, 1500);
//Zinc Ore

//Poor Silver ore
mods.thermalexpansion.Pulverizer.addRecipe(nuggetSilver *2, orePoorSilver, 1500);
//Poor Silver ore

//Poor Gold ore
mods.thermalexpansion.Pulverizer.addRecipe(nuggetGold *2, orePoorGold, 1500);
//Poor Gold Ore

//Poor Lead Ore
mods.thermalexpansion.Pulverizer.addRecipe(nuggetLead *2, orePoorLead, 1500);
//Poor Lead Ore

//Poor Copper Ore
mods.thermalexpansion.Pulverizer.addRecipe(nuggetCopper *2, orePoorCopper, 1500);
//Poor Copper Ore

//Poor Iron Ore
mods.thermalexpansion.Pulverizer.addRecipe(nuggetIron *2, orePoorIron, 1500);
//Poor Iron Ore

//Poor Tin Ore
mods.thermalexpansion.Pulverizer.addRecipe(nuggetTin *2, orePoorTin, 1500);
//Poor Tin Ore

//Poor Nickel ore
mods.thermalexpansion.Pulverizer.addRecipe(nuggetNickel *2, orePoorNickel, 1500);
//Poor Nickel ore

//Broken Ores

//MCA
//Villager Editor
recipes.addShapeless("Villager Editor", <mca:villager_editor>, [<mca:rose_gold_dust>, <mca:gold_dust>, <minecraft:writable_book>]);
//Villager Editor
//MCA

